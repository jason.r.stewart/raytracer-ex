# RaytracerEx

A physically-based graphics rendering engine written in Elixir.

## Usage

**TODO:** Add usage documentation when the command line interface is complete.

## Running tests

Clone the repo and fetch its dependencies:

```
git clone https://github.com/jrstewart/raytracer_ex.git
cd raytracer_ex
mix deps.get
mix test
```

## Running Dialyzer

After cloning the repo, run the following:

```
mix dialyzer
```
